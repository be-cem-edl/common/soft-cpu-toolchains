#!/bin/bash
set -e

NEWLIB_V=3.0.0
GCC_V=11.2.0
GDB_V=11.2
BINUTILS_V=2.38
MPC_V=1.0.3
MPFR_V=3.1.4
GMP_V=6.1.0
ISL_V=0.18

JOBS=`nproc`

CUR_DIR=`pwd`

PREFIX=${CUR_DIR}/riscv-toolchain

TMP_DIR=${CUR_DIR}/_tmp_install

CACHE_DIR=${CUR_DIR}/_cache

BUILD_DIR=${CUR_DIR}/_build

_check_and_get()
{
    pushd ${CACHE_DIR} > /dev/null
    wget $1 -nc
    popd > /dev/null
}

do_download()
{
    _check_and_get ftp://ftp.gnu.org/gnu/binutils/binutils-${BINUTILS_V}.tar.xz
    _check_and_get ftp://ftp.gnu.org/gnu/gdb/gdb-${GDB_V}.tar.gz
    _check_and_get ftp://ftp.gnu.org/gnu/gmp/gmp-${GMP_V}.tar.bz2
    _check_and_get ftp://ftp.gnu.org/gnu/mpfr/mpfr-${MPFR_V}.tar.bz2
    _check_and_get ftp://ftp.gnu.org/gnu/mpc/mpc-${MPC_V}.tar.gz
    _check_and_get ftp://gcc.gnu.org/pub/gcc/infrastructure/isl-${ISL_V}.tar.bz2
    _check_and_get ftp://ftp.gnu.org/gnu/gcc/gcc-${GCC_V}/gcc-${GCC_V}.tar.xz
    _check_and_get ftp://sourceware.org/pub/newlib/newlib-${NEWLIB_V}.tar.gz
}

_check_and_extract()
{
    pushd ${CACHE_DIR} > /dev/null
    ARCHIVE=$1
    DIR_NAME=${ARCHIVE%.tar.*}
    if [ -d ${DIR_NAME} ]
    then
	echo "Directory '${DIR_NAME}' already there; not extracting"
    else
	tar -x -f $1
    fi
    popd > /dev/null
}

do_extract()
{
    _check_and_extract gdb-${GDB_V}.tar.gz
    _check_and_extract binutils-${BINUTILS_V}.tar.xz
    _check_and_extract gmp-${GMP_V}.tar.bz2
    _check_and_extract mpfr-${MPFR_V}.tar.bz2
    _check_and_extract mpc-${MPC_V}.tar.gz
    _check_and_extract isl-${ISL_V}.tar.bz2
    _check_and_extract gcc-${GCC_V}.tar.xz
    _check_and_extract newlib-${NEWLIB_V}.tar.gz
}

do_gdb()
{
    GDB_BUILD_DIR=${BUILD_DIR}/gdb-${GDB_V}
    rm -rf ${GDB_BUILD_DIR}
    mkdir -p ${GDB_BUILD_DIR}

    pushd ${GDB_BUILD_DIR} > /dev/null

    ${CACHE_DIR}/gdb-${GDB_V}/configure --prefix=${PREFIX} --target=riscv32-elf \
		--with-libgmp-prefix=${TMP_DIR} --with-libmpfr-prefix=${TMP_DIR}
    make -j${JOBS}
    make install

    popd > /dev/null
}

do_binutils()
{
    BINUTILS_BUILD_DIR=${BUILD_DIR}/binutils-${BINUTILS_V}
    rm -rf ${BINUTILS_BUILD_DIR}
    mkdir -p ${BINUTILS_BUILD_DIR}

    pushd ${BINUTILS_BUILD_DIR} > /dev/null

    ${CACHE_DIR}/binutils-${BINUTILS_V}/configure --prefix=${PREFIX} --target=riscv32-elf \
		--disable-readline \
		--disable-libdecnumber \
		--disable-sim \
		--disable-nls
    make -j${JOBS}
    make install-strip

    popd > /dev/null
}

do_gmp()
{
    GMP_BUILD_DIR=${BUILD_DIR}/gmp-${GMP_V}
    rm -rf ${GMP_BUILD_DIR}
    mkdir -p ${GMP_BUILD_DIR}

    pushd ${GMP_BUILD_DIR} > /dev/null

    # Be very conservative about the cpu to support installation on a different host.
    ${CACHE_DIR}/gmp-${GMP_V}/configure --prefix=${TMP_DIR} --host=core2-pc-linux-gnu \
		--disable-shared
    make -j${JOBS}
    make check
    make install

    popd > /dev/null
}

do_mpfr()
{
    MPFR_BUILD_DIR=${BUILD_DIR}/mpfr-${MPFR_V}
    rm -rf ${MPFR_BUILD_DIR}
    mkdir -p ${MPFR_BUILD_DIR}

    pushd ${MPFR_BUILD_DIR} > /dev/null

    ${CACHE_DIR}/mpfr-${MPFR_V}/configure --prefix=${TMP_DIR} \
		--with-gmp=${TMP_DIR} \
		--disable-shared
    make -j${JOBS}
    make check
    make install

    popd > /dev/null
}

do_mpc()
{
    MPC_BUILD_DIR=${BUILD_DIR}/mpc-${MPC_V}
    rm -rf ${MPC_BUILD_DIR}
    mkdir -p ${MPC_BUILD_DIR}

    pushd ${MPC_BUILD_DIR} > /dev/null

    ${CACHE_DIR}/mpc-${MPC_V}/configure --prefix=${TMP_DIR} \
		--with-gmp=${TMP_DIR} \
		--disable-shared
    make -j${JOBS}
    make check
    make install

    popd > /dev/null
}

do_isl()
{
    ISL_BUILD_DIR=${BUILD_DIR}/isl-${ISL_V}
    rm -rf ${ISL_BUILD_DIR}
    mkdir -p ${ISL_BUILD_DIR}

    pushd ${ISL_BUILD_DIR} > /dev/null

    ${CACHE_DIR}/isl-${ISL_V}/configure --prefix=${TMP_DIR} \
		--with-gmp-prefix=${TMP_DIR} \
		--disable-shared
    make -j${JOBS}
    make check
    make install

    popd > /dev/null
}

do_gcc()
{
    GCC_BUILD_DIR=${BUILD_DIR}/gcc-${GCC_V}
    rm -rf ${GCC_BUILD_DIR}
    mkdir -p ${GCC_BUILD_DIR}

    pushd ${GCC_BUILD_DIR} > /dev/null

    ${CACHE_DIR}/gcc-${GCC_V}/configure --prefix=${PREFIX} --target=riscv32-elf \
	--enable-multilib \
	--enable-languages=c,c++ \
	--disable-nls \
	--disable-shared \
	--disable-libssp \
	--disable-libstdcxx \
	--with-gmp=${TMP_DIR}
    make -j${JOBS}
    make install

    popd > /dev/null
}

do_newlib()
{
    NEWLIB_BUILD_DIR=${BUILD_DIR}/newlib-${NEWLIB_V}
    rm -rf ${NEWLIB_BUILD_DIR}
    mkdir -p ${NEWLIB_BUILD_DIR}

    pushd ${NEWLIB_BUILD_DIR} > /dev/null

    ${CACHE_DIR}/newlib-${NEWLIB_V}/configure --prefix=${PREFIX} --target=riscv32-elf \
	--with-newlib \
	--enable-newlib-nano-malloc \
	--disable-newlib-multithread \
	CC_FOR_TARGET=${PREFIX}/bin/riscv32-elf-gcc \
	AR_FOR_TARGET=${PREFIX}/bin/riscv32-elf-ar \
	RANLIB_FOR_TARGET=${PREFIX}/bin/riscv32-elf-ranlib
    make -j${JOBS}
    make install

    popd > /dev/null
}

do_all()
{
    do_download
    do_extract
    do_gmp
    do_mpfr
    do_mpc
    do_isl
    do_gdb
    do_binutils
    do_gcc
    do_newlib
    do_gcc
}

mkdir -p ${CACHE_DIR}

mkdir -p ${BUILD_DIR}

mkdir -p ${TMP_DIR}

mkdir -p ${PREFIX}

if [ $# = 1 ]; then
    eval do_$1
elif [ $# = 0 ]; then
    do_all
else
    echo "usage: $0 ACTION"
    exit 1
fi
